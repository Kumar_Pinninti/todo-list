// declaring and assigning selectors

const todoInput = document.querySelector('.todo-input');
const todoButton = document.querySelector('.todo-button');
const todoList = document.querySelector('.todo-list');
const filterOption = document.querySelector('.filter-todo');




// add event listeners

document.addEventListener("DOMContentLoaded", getTodos);

todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteCheck);
filterOption.addEventListener('change', filterTodo);



// functions


function addTodo(event){

    // add prevent default to avoid browser auto refresh
    event.preventDefault();

    // creating todo div
    const todoDiv = document.createElement('div');
    todoDiv.classList.add('todo');

    //create li tag
    const newTodo = document.createElement('li');
    newTodo.innerText = todoInput.value;
    newTodo.classList.add('todo-item');
    todoDiv.appendChild(newTodo);

    //add todo to local storage
    saveTodosInLocal(todoInput.value);

    // create checked button
    const checkedButton = document.createElement('button');
    checkedButton.innerHTML = '<i class="fas fa-check"></i>'
    checkedButton.classList.add('checked-btn');
    todoDiv.appendChild(checkedButton);

    // create delete button
    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.classList.add('delete-btn');
    todoDiv.appendChild(deleteButton);


    // append all created elements to todo-list
    todoList.appendChild(todoDiv);

    //clear todo input

    todoInput.value = '';
}


function deleteCheck(event){
    const item = event.target;

    //delete

    if(item.classList[0] === 'delete-btn'){
        const todo = item.parentElement;
        todo.classList.add('fall');

        // removing todos from local storage
        removeTodosInLocal(todo);

        // removing from browser after animation
        todo.addEventListener('transitionend', function(){
            todo.remove();
        })
    }


    // checked

    if(item.classList[0] === 'checked-btn'){
        const todo = item.parentElement;
        todo.classList.toggle('checked');
    }
}


function filterTodo(event){
    const todos = todoList.childNodes;

    todos.forEach((todo) => {
        switch(event.target.value){

            case "all":
                todo.style.display = "flex";
                break;
            
            case "completed":
                if(todo.classList.contains("checked")){
                    todo.style.display = "flex";
                }
                else{
                    todo.style.display = "none";
                }
                break;

            case "pending":
                if(!todo.classList.contains("checked")){
                    todo.style.display = "flex";
                }
                else{
                    todo.style.display = 'none';
                }
                break;

            default: break;
        }
    });
}


// saving in local storage

function saveTodosInLocal(todo){
    let todos;

    if(localStorage.getItem("todos") === null){
        todos = [];
    }
    else{
        todos = JSON.parse(localStorage.getItem("todos"));
    }

    todos.push(todo);
    localStorage.setItem("todos", JSON.stringify(todos));
}


function getTodos(){
    let todos;

    if(localStorage.getItem("todos") === null){
        todos = [];
    }
    else{
        todos = JSON.parse(localStorage.getItem("todos"));
    }

    todos.forEach((todo) => {
        // creating todo div
        const todoDiv = document.createElement('div');
        todoDiv.classList.add('todo');

        //create li tag
        const newTodo = document.createElement('li');
        newTodo.innerText = todo;
        newTodo.classList.add('todo-item');
        todoDiv.appendChild(newTodo);

        // create checked button
        const checkedButton = document.createElement('button');
        checkedButton.innerHTML = '<i class="fas fa-check"></i>'
        checkedButton.classList.add('checked-btn');
        todoDiv.appendChild(checkedButton);

        // create delete button
        const deleteButton = document.createElement('button');
        deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
        deleteButton.classList.add('delete-btn');
        todoDiv.appendChild(deleteButton);


        // append all created elements to todo-list
        todoList.appendChild(todoDiv);
    });
}


// remove todos from local storage

function removeTodosInLocal(todo){
    let todos;

    if(localStorage.getItem("todos") === null){
        todos = [];
    }
    else{
        todos = JSON.parse(localStorage.getItem("todos"));
    }

    const todoIndex = todo.children[0].innerText;
    todos.splice(todos.indexOf(todoIndex), 1); // 1 means removing 1 element from array
    localStorage.setItem("todos", JSON.stringify(todos));
}